import java.awt.Graphics;
import javax.swing.JPanel;
import javax.swing.JOptionPane;

public class DrawPanel extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		int width = getWidth();
		int height = getHeight();
		final int AMOUNT =5;
		
		for (int i= 1; i<=AMOUNT;i++ )
		{
			String input = JOptionPane.showInputDialog("Enter an integer between 1 and 30:");
			
			// for the 5 numbers entered by the user

			int current = Integer.parseInt(input);
									
			g.drawRect(0,(i-1)* height/10, current*width/30, height/10);
			
			
		}
		
		
		
	}
}
